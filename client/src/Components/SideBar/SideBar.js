import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import './SideBar.css'

export default function SideBar() {

    const [cat, setCat] = useState([]);
    
    useEffect(() => {
        const getCategories = async() => {
            const res = await axios.get('/categories/');
            setCat(res.data);
        }
        getCategories();
    }, [])

    return (
        <div className="SideBar">
            <div className="SideBar_Item">
                <span className="SideBar_Title">ABOUT ME</span>
                <img 
                    className="SideBar_Image"
                    src="https://images.unsplash.com/photo-1612151855475-877969f4a6cc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8aGQlMjBpbWFnZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60"
                    alt=""
                />
                <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam quidem voluptatum fugit id repudiandae 
                </p>
            </div>
            <div className="SideBar_Item">
                <span className="SideBar_Title">CATEGORIES</span>
                <ul className="SideBar_List">
                    {cat.map((obj, key) => {
                        return (
                            <Link className="link" to={`/?categories=${obj.name}`} key={key}>
                            <li className="SideBar_ListItem">{obj.name}</li>
                            </Link>
                        )
                    })}
                </ul>
            </div>
            <div className="SideBar_Item">
                <span className="SideBar_Title">FOLLOW US</span>
                <div className="SideBar_Social">
                    <i className="SideBar_Icon fa-brands fa-twitter"></i>
                </div>
            </div>
        </div>
    )
}
