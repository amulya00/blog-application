import React from 'react'
import { Link } from 'react-router-dom'
import './Post.css'

export default function Post({post}) {

  return (
    <div className="Post">
        <Link className="link" to={`/post/${post._id}`}>
        <img 
            className="Post_Image"
            src={post.photo?post.photo:""}
            alt=""
        />
        <div className="Post_Info">
            <div className="Post_Categories">
                {post.categories && post.categories.map((obj, key) => (
                    <span className="Post_Category" key={key}>{obj}</span>
                ))}
            </div>
            <span className="Post_Title">{post.title}</span>
            <span style={{fontSize:"16px", marginBottom: "10px"}} className="Post_Title"> By {post.username}</span>
            <hr/>
            <span className="Post_Date">{new Date(post.createdAt).toDateString()}</span>
        </div> 
        <p className="Post_Description">
            {post.desc}
        </p></Link>
    </div>
  )
}
