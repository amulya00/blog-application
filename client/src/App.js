import React, { useContext } from 'react';
import './App.css';

import HomePage from './Pages/HomePage/HomePage';
import TopBar from './Components/TopBar/TopBar';
import PostPage from './Pages/PostPage/PostPage';
import Write from './Pages/Write/Write';
import Settings from './Pages/Settings/Settings';
import Login from './Pages/Login/Login';
import Register from './Pages/Register/Register';

import { 
  BrowserRouter as Router,
  Route,
  Routes
} from "react-router-dom";
import { Context } from './Context/Context';

function App() {
  const {user} = useContext(Context);
  return (
    <div className="App">
      <Router>
        <TopBar />
        <Routes>
          <Route path="/" element={<HomePage />}/>  
          <Route path="/post/:id" element={<PostPage />} />
          <Route path="/write" element={user?<Write />:<Login />}/>
          <Route path="/settings" element={user?<Settings />:<Login />}/>
          <Route path="/login" element={user?<HomePage />:<Login />}/>
          <Route path="/register" element={user?<HomePage />:<Register />}/>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
