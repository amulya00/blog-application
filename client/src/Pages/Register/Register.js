import React, { useEffect, useState } from 'react'
import axios from "axios";
import { Link, useNavigate } from 'react-router-dom'
import './Register.css'

export default function Register() {
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("")
  const [err, setErr] = useState(false);

  const register = async (e) => {
    e.preventDefault();
    setErr(false);
    // localhost:5000/api/auth/register
    try{
      const res = await axios.post("/auth/register", {
        username, email, password
      })
      if(res.status === 200){
        navigate('/login')
      }
    }catch(err){
      setErr(true);
      console.log(err);
    }
  }

  return (
      <div className="Register">
        <span className="Register_Title">Register</span>
        <form action="" className="Register_Form">
            <label>Username : </label>
            <input 
              type="text" 
              placeholder='Enter your username ....'
              value={username}
              required={true}
              onChange={(e) => setUsername(e.target.value)}
            />
            <label>Email : </label>
            <input 
              type="email" 
              required={true}
              placeholder='Enter your email ....' 
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <label>Password : </label>
            <input 
              type="password" 
              placeholder='Enter your password ....' 
              value={password}
              required={true}
              onChange={(e) => setPassword(e.target.value)}  
            />
            <button className="Register_Login_Button" onClick={register}>
              Register
            </button>
            {err && <span style={{color:"red", marginTop:"18px"}}>Something Went Wrong!</span>}
            <button className="Register_Register_Button">
              <Link className="link" to="/login">Login</Link>
            </button>
        </form>
      </div>
  )
}
