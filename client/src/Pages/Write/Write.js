import React, {useContext, useRef, useState} from 'react';
import axios from "axios"
import {Context} from '../../Context/Context'
import './Write.css'

export default function Write() {
    const [title, setTitle] = useState("");
    const [desc, setDesc] = useState("");
    const [file, setFile] = useState(null);

    const {user} = useContext(Context);
    const photo = useRef();
    const handleSubmit = async (e) => {
        
        e.preventDefault();
        const data = new FormData();
        console.log("submit")
        const newPost = {username: user.username, title, desc};
        console.log(newPost, file);
        
        if(file){
            
            const filename = Date.now().toString() + user.username;
            data.append("name", filename);
            data.append("file", file);
            newPost.photo = filename;
            try{
                const res = await axios.post('/fileUpload', data)
                photo.current = res.data.path;
            }catch(err){
                console.log(err);
            }
        }
        try{
            const data = {title, desc, photo:photo.current, username: user.username}
            const res = await axios.post('/posts', data);
            console.log(res);   
        }catch(err){
            console.log(err);
        }
    }

    return (
        <div className="Write">
            <img
                className="Write_Image"
                src={file ? URL.createObjectURL(file) : ""}
                alt=""
            />
            <form className="Write_Form" onSubmit={handleSubmit}>
                <div className="Write_Form_Group">
                    <label htmlFor="File_Input">
                        <i className="Write_Icon fas fa-plus"></i>
                    </label>
                    <input type="file" id="File_Input" style={{display: "none"}} onChange={(e) => setFile(e.target.files[0])}/>
                    <input type="text" placeholder='Title' className="Write_Input" autoFocus={true} value={title} onChange={(e) => setTitle(e.target.value)}/> 
                </div>
                <div className="Write_Form_Group">
                    <textarea placeholder='Tell your story....' type="text" className="Write_Input Write_Text" value={desc} onChange={(e) => setDesc(e.target.value)}>
                    </textarea>
                </div>
                <button className="Write_Submit" type="submit">Publish</button>
            </form>
        </div>
    )
}
