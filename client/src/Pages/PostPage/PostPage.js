import React from 'react';
import SideBar from '../../Components/SideBar/SideBar';
import SinglePost from '../../Components/SinglePost/SinglePost';
import './PostPage.css';

export default function PostPage() {
  return (
    <div className="PostPage">
        <SinglePost />
        <SideBar />
    </div>
  )
}
