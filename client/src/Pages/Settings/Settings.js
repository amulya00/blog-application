import React from 'react'

import SideBar from '../../Components/SideBar/SideBar'
import './Settings.css'

export default function Settings() {
  return (
    <div className="Settings">
      <div className="Settings_Wrapper">
        <div className="Settings_Title">
          <span className="Settings_Update_Title">Update Your Account</span>
          <span className="Settings_Delete_Title">Delete Your Account</span>
        </div>
        <form action="" className="Settings_Form">
          <label>Profile Picture</label>
          <div className="Settings_PP">
            <img
              src="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg"
              alt=""
            />
            <label for="FileInput">
              <i className="Settings_PP_Icon far fa-user-circle">
              </i>
            </label>
            <input type="file" id="FileInput" style={{display:"none"}} />
          </div>
          <label>UserName: </label>
          <input type="text" placeholder="ABC" />
          <label>Email: </label>
          <input type="email" placeholder="test@123" />
          <label>Password: </label>
          <input type="password"/>
          <button className="Settings_Submit">Update </button>
        </form>
      </div>
      <SideBar />
    </div>
  )
}
