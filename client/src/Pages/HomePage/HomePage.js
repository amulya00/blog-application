import React from 'react'
import Header from '../../Components/Header/Header.js'
import Posts from '../../Components/Posts/Posts.js'
import SideBar from '../../Components/SideBar/SideBar.js'
import './HomePage.css'

export default function HomePage() {
  return (
    <div>
        <Header />
        <div className="HomePage">
          <Posts />
          <SideBar />
        </div>
    </div>
  )
}
